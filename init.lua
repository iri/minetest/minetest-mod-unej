minetest.register_privilege("u_grant",{description = "unrestricted use of manage_privs commands" , give_to_singleplayer = false})
minetest.register_privilege("u_give",{description = "unrestricted use of manage_give commands" , give_to_singleplayer = false})
unej = {}

unej.history = nil
local file = io.open(minetest.get_worldpath() .. "/schems/history/history.dat","r")
if file then
  unej.history = minetest.parse_json(file:read("*all"))
  file:close()
else minetest.mkdir(minetest.get_worldpath() .. "/schems/history/")
end
if not unej.history then
  unej.history = {}
end

local ml = minetest.settings:get("unej.max_lengths")
local mu = minetest.settings:get("unej.max_undo")
unej.max_lengths = ml and minetest.parse_json(ml) or {x=256, y=256, z=256}
unej.history_max_undo = mu and tonumber(mu) or 10

unej.conf_path = minetest.get_worldpath()
unej.modpath = minetest.get_modpath("unej")
dofile(unej.modpath.."/utils.lua")
dofile(unej.modpath.."/manage_privs.lua")
dofile(unej.modpath.."/manage_world.lua")
dofile(unej.modpath.."/undo.lua")
dofile(unej.modpath.."/worldedit.lua")
dofile(unej.modpath.."/manage_give.lua")
dofile(unej.modpath.."/manage_teleport.lua")


minetest.register_on_joinplayer(unej.remove_dangling_privs)
