local function check_region(name)
	return worldedit.volume(worldedit.pos1[name], worldedit.pos2[name])
end

worldedit.register_command("bigmtschemcreate", {
	params = "<folder_name>",
	description = "Save the current WorldEdit region using the Minetest "..
		"Schematic format to \"(world folder)/schems/<folder_name>/*_*_*.mts\"",
	privs = {worldedit=true},
	require_pos = 2,
	parse = function(param)
		if param == "" then
			return false
		end
		--if not check_filename(param) then
		--	return false, "Disallowed file name: " .. param
		--end
		return true, param
	end,
	nodes_needed = check_region,
	func = function(name, param)
	local path = minetest.get_worldpath() .. "/schems/".. param
	-- Create directory if it does not already exist
	minetest.mkdir(path)

	get_len = function(name,i)
		if (length_region[name]-(i*unej.max_lengths[name])) > 0 then
			return unej.max_lengths[name]-1
		else
			return length_region[name]-((i-1)*unej.max_lengths[name])
		end
	end
	data = {}
	local pos1 = {}
	local pos2 = {}
	local pos_min = { x=math.min(worldedit.pos1[name].x,worldedit.pos2[name].x), y=math.min(worldedit.pos1[name].y,worldedit.pos2[name].y), z=math.min(worldedit.pos1[name].z,worldedit.pos2[name].z)}
	local pos_max = { x=math.max(worldedit.pos1[name].x,worldedit.pos2[name].x), y=math.max(worldedit.pos1[name].y,worldedit.pos2[name].y), z=math.max(worldedit.pos1[name].z,worldedit.pos2[name].z)}

	length_region = {x = pos_max.x-pos_min.x, y = pos_max.y-pos_min.y, z = pos_max.z-pos_min.z, is_bigmtschm=true}
	table.insert(data,length_region)

	for i = 1, math.ceil(length_region.x/unej.max_lengths.x) do
		len_x = get_len("x",i)
		pos1.x = pos_min.x + (i-1)*unej.max_lengths.x
		pos2.x = pos1.x + len_x
		for j = 1, math.ceil(length_region.y/unej.max_lengths.y) do
			len_y = get_len("y",j)
			pos1.y = pos_min.y + (j-1)*unej.max_lengths.y
			pos2.y = pos1.y + len_y
			for k = 1, math.ceil(length_region.z/unej.max_lengths.z) do
				len_z = get_len("z",k)
				pos1.z = pos_min.z + (k-1)*unej.max_lengths.z
				pos2.z = pos1.z + len_z
				local fname = i.."_"..j.."_"..k..".mts"
				local filename = path .. "/"..fname
				local schm = minetest.create_schematic(pos1, pos2, nil, filename)
				if schm == nil then
					worldedit.player_notify(name, "Failed to create Minetest schematic", false)
					return
				end
				local pos1_save = {x=pos1.x-pos_min.x, y=pos1.y-pos_min.y, z=pos1.z-pos_min.z}
				local pos2_save = {x=pos2.x-pos_min.x, y=pos2.y-pos_min.y, z=pos2.z-pos_min.z}
				table.insert(data, {pos_min=pos1_save, pos_max=pos2_save, nom=fname})
			end
		end
	end

	if not unej.write_file(path .. "/" .. "data",data) then
		worldedit.player_notify(name, "The function occured an error, the data file is wrong")
		minetest.chat_send_player(name, unej.table_unpack(data))
	else
		worldedit.player_notify(name, "Saved Minetest schematic to " .. param)
	end
	worldedit.prob_list[name] = {}
	end,
})

worldedit.register_command("bigmtschemplace", {
	params = "<folder>",
	description = "Load nodes from \"(world folder)/schems/<folder>.mts\" with position 1 of the current WorldEdit region as the origin",
	privs = {worldedit=true},
	require_pos = 1,
	parse = function(param)
		if param == "" then
			return false
		end
		-- if not check_filename(param) then
		--	return false, "Disallowed file name: " .. param
		--end
		return true, param
	end,
	func = function(name, param)
		local pos_i = worldedit.pos1[name]
		local path = minetest.get_worldpath() .. "/schems/" .. param .."/"
		local data = unej.open_file( path.."data")
		if not data then
			worldedit.player_notify(name,"Erreur du chargement de "..path.."data\nSi l'erreur persiste, vérifiez l'intégrité du fichier.")
			minetest.log("error", "UNEJ mod: Error when loading "..path.."data.")
			return false
		end
		if not data[2].pos_max then
			minetest.chat_send_player(name,"UNEJ mod -!- Erreur, le dossier spécifié a été créé depuis une version antérieure de ce mod et ne peut pas être utilisé.")
			return false
		end
		if not data[1].is_bigmtschm then
			worldedit.player_notify(name,"Erreur, le dossier spécifié n'est pas un fichier //bigmtschemcreate.")
			return false
		end
		local cmd = "//bigmtschemplace "..param..unej.table_unpack(pos_i)
		local history_path = minetest.get_worldpath() .. "/schems/history/"
		unej.history_switch(cmd)
		os.remove(history_path..unej.history[1].folder_name)
		minetest.mkdir(history_path..unej.history[1].folder_name)
		local history_path = minetest.get_worldpath() .. "/schems/history/"
		local length_region = {x=data[1].x, y=data[1].y, z=data[1].z}

		for i,v in ipairs(data) do
			if i ~= 1 then
				local pos = {x=v.pos_min.x + pos_i.x, y = v.pos_min.y + pos_i.y, z=v.pos_min.z + pos_i.z}
				local pos2 = {x=v.pos_max.x + pos_i.x, y = v.pos_max.y + pos_i.y, z=v.pos_max.z + pos_i.z}
				minetest.create_schematic(pos, pos2, nil, history_path..unej.history[1].folder_name.."/".. #unej.history[1].pos + 1 ..".mts")
				table.insert(unej.history[1].pos, pos)
				if minetest.place_schematic(pos, path..v.nom) == nil then
					worldedit.player_notify(name, "failed to place Minetest schematic")
					return
				end
			end
		end
		unej.write_file(minetest.get_worldpath() .. "/schems/history/history.dat",unej.history)
		worldedit.player_notify(name, "placed Minetest schematic " .. param)
	end,
})

worldedit.register_command("protect", {
	params = "<area_name>",
	description = "Save the current WorldEdit region using the Minetest "..
		"Schematic format to \"(world folder)/schems/<folder_name>/*_*_*.mts\"",
	privs = {unej_admin=true},
	require_pos = 2,
	parse = function(param)
		if param == "" then
			return false
		end
		return true, param
	end,
	nodes_needed = check_region,
	func = function(name, param)
		unej.protect(worldedit.pos1[name],worldedit.pos2[name],name,param,false)
	end,
})
