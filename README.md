*English version below*

# Minetest mod unej (FR)


### À propos de ce mod
Ce mod a été créé en 2019 au sein de l’[Institut de recherche et d’innovation](https://www.iri.centrepompidou.fr/) à Paris afin de répondre à la nécessité de faciliter l’utilisation de Minetest pour la gestion de groupes d’élèves dans le cadre du projet UNEJ : [Urbanités numériques en jeux](https://unej.tac93.fr/). C’est un projet lancé dans le cadre d’un [programme de recherche](https://tac93.fr/) se déroulant en Seine-Saint-Denis (dans le nord de Paris), sous l’impulsion du philosophe Bernard Stiegler, qui vise à permettre à des élèves de collèges et lycées de se réapproprier leur territoire à travers Minetest : le but étant qu’ils travaillent avec [Minetest](https://www.minetest.net/education/) pour repenser l’aménagement de leur cour de récréation ou le quartier autour de leur établissement scolaire (ce qui est rendu possible par l’[IGN](https://minecraft.ign.fr/) qui laisse accès à des reproductions en version Minetest de cartes de tout le territoire français jusqu’à 5km2, et qui, pour le projet UNEJ a généré des cartes de 25km2, laissant ainsi à disposition des élèves un espace de jeu couvrant tout leur territoire - dans lequel se trouve aussi l’espace du village Olympique des jeux de Paris).


### Suites du projet
Ce mod a d’une part pour vocation d’être archivé, maintenant que le projet a été mené à son terme, mais il s’agit aussi de le refactoriser afin d’en extraire les fonctionnalités pouvant resservir dans le cadre d’un usage éducatif, mais aussi en général. En l’état, ce mod a servi à différentes fonctions, il a offert : 
La possibilité d'utiliser des commandes de groupes (donner ou enlever des objets à plusieurs personnes, donner ou retirer des droits à plusieurs personnes, téléporter plusieurs personnes) en ajoutant des commandes qui fonctionnent en lien avec le mod [playerfactions](https://github.com/mt-mods/playerfactions)
La possibilité d'importer et exporter de gros modèles Minetest (schematics) en étendant les fonctionnalités venant des mods [worldedit](https://github.com/Uberi/Minetest-WorldEdit/) et [worldedit_undo](https://github.com/HybridDog/we_undo) (undo permet d'annuler des commandes) [ces fonctionnalités pourraient être extraites et ajoutées au mods worldedit et worldedit_undo]
La possibilité d'enregistrer un monde entier en schematics et le réimporter dans un autre monde en le plaçant selon ses données géographiques, en le protégeant et en l'entourant de blocs transparents. 
Pour voir le détail des différentes commandes, vous pouvez regarder le document [COMMANDS.md](https://gitlab.com/iri-research-org/urbanum/minetest/minetest-mod-unej/-/blob/master/COMMANDS.md)


### Contributeurs
Ce mod a été développé au sein de l’Institut de recherche et d’innovation (IRI) :

Direction et développement : Yves-Marie Haussonne https://github.com/ymph, https://gitlab.com/ymph 

Développement : Basile Leretaille https://github.com/Kalio-42, https://github.com/Supergoat666, https://gitlab.com/Kalio42  
Compléments de développement : Ulysse Prince https://github.com/uprince, https://gitlab.com/uprince

Tests d’usage : Riwad Salim https://github.com/late-coffee, https://gitlab.com/Late-Coffee 

Mise en page et rédaction du readme : Merlin Payant





# Minetest mod unej (EN)

### About this mod
This mod was created in 2019 at the [Institute for Research and Innovation](https://www.iri.centrepompidou.fr/) in Paris, facilitating the use of Minetest for managing student groups as part of the UNEJ project: [Urbanités numériques en jeux](https://unej.tac93.fr/). This project launched as part of a [contributory research program](https://tac93.fr/) taking place in Seine-Saint-Denis (suburb north of Paris), under the impetus of philosopher Bernard Stiegler. UNEJ enables middle and high school students to reclaim their neighborhoods through Minetest, empowering them to use the application [Minetest](https://www.minetest.net/education/) to reimagine and redesign their community surroundings, including their playgrounds and their district’s community spaces. This is made possible by [IGN](https://minecraft.ign.fr/), which equips Minetest with map reproductions of all of France, with up to 5km^2 in precision. For the UNEJ project, IGN has generated maps of 25km^2, providing pupils with interactive Minetest maps covering their entire territories - including the area of the Olympic Village for the Paris Games.

### Project follow-up
On one hand, this mod is intended to archive now that the project has been completed, but it is may also be accessed for educational purposes, and for general interest. As it stands, this mod has served a number of functions, including :
The ability to use group commands (give or remove items to people, give or remove modification rights to people, teleport several people) by adding commands that work in conjunction with the mod [playerfactions](https://github.com/mt-mods/playerfactions)
The ability to import and export large Minetest models (schematics) by extending the functionalities of the [worldedit](https://github.com/Uberi/Minetest-WorldEdit/) and [worldedit_undo](https://github.com/HybridDog/we_undo) mods (undo allows commands to be canceled) [these functionalities could be extracted and added to the worldedit and worldedit_undo mods].
The ability to save an entire world in schematics and import it into another world, placing it according to its geographical data, protecting it and surrounding it with transparent blocks.
To see the details of the various commands, please refer to the [COMMANDS.md](https://gitlab.com/iri-research-org/urbanum/minetest/minetest-mod-unej/-/blob/master/COMMANDS.md) document.

### Contributors
This mod was developed at the Institute for Research and Innovation (IRI):

Direction and development: Yves-Marie Haussonne https://github.com/ymph, https://gitlab.com/ymph

Development: Basile Leretaille https://github.com/Kalio-42, https://github.com/Supergoat666, https://gitlab.com/Kalio42  

Additional development: Ulysse Prince https://github.com/uprince, https://gitlab.com/uprince

Use tests: Riwad Salim https://github.com/late-coffee, https://gitlab.com/Late-Coffee

Layout and readme: Merlin Payant