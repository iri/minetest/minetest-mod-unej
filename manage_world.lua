unej.wmtscreate = function(name,params)
	local folder_name = params[1]
	local path = minetest.get_worldpath() .. "/schems/".. folder_name
	-- Create directory if it does not already exist
	minetest.mkdir(path)

	get_len = function(name,i)
		if (length_region[name]-(i*unej.max_lengths[name])) > 0 then
			return unej.max_lengths[name]-1
		else
			return length_region[name]-((i-1)*unej.max_lengths[name])
		end
	end

	local pos1 = {}
	local pos2 = {}

	local geometry = unej.open_file( minetest.get_worldpath().."/geometry.dat")
	if not geometry then
		minetest.chat_send_player(name,"UNEJ mod -!- Erreur du chargement de worldpath/geometry.dat\nSi l'erreur persiste, vérifiez l'intégrité du fichier.")
		minetest.log("error", "UNEJ mod: Error when loading worldpath/geometry.dat.")
		return false
	end

	local pos_min = {}
	pos_min.x = geometry.coordinatesGame[4][1]
	if params[2] == nil or not tonumber(params[2]) then
		pos_min.y = 20
	else
		pos_min.y = tonumber(params[2])
	end
	pos_min.z = geometry.coordinatesGame[4][2]
	local pos_max = {}
	pos_max.x = geometry.coordinatesGame[2][1]
	if params[3] == nil or not tonumber(params[3]) then
		pos_max.y = 148
	else
		pos_max.y = tonumber(params[3])
	end
	pos_max.z = geometry.coordinatesGame[2][2]

	if pos_max.y < pos_min.y then
		minetest.chat_send_player(name, "IRI -!- La position minimale a une altitude supérieure à celle de la position maximale, cela est surement du à un problème dans vos paramètres.\nVeuillez réessayer avec des valeurs cohérentes")
		return
	end
	local translation_X = (geometry.coordinatesCarto[1][1] +  geometry.coordinatesCarto[3][1])/2
	local translation_Z = (geometry.coordinatesCarto[1][2] +	geometry.coordinatesCarto[3][2])/2
	minetest.chat_send_player(name, "translat X : "..translation_X)
	minetest.chat_send_player(name, "translat Z : "..translation_Z)

	length_region = {x = pos_max.x - pos_min.x, y = pos_max.y-pos_min.y, z = pos_max.z-pos_min.z}

	minetest.chat_send_player(name, "UNEJ mod -!- The creation begin...")
	data = {}
	table.insert(data, {x=pos_max.x + translation_X, y=length_region.y, z=pos_max.z + translation_Z, is_world=true})

	for i = 1, math.ceil(length_region.x/unej.max_lengths.x) do
		local len_x = get_len("x",i)
		pos1.x = pos_min.x + (i-1)*unej.max_lengths.x
		pos2.x = pos1.x + len_x
		for j = 1, math.ceil(length_region.y/unej.max_lengths.y) do
			local len_y = get_len("y",j)
			pos1.y = pos_min.y + (j-1)*unej.max_lengths.y
			pos2.y = pos1.y + len_y
			for k = 1, math.ceil(length_region.z/unej.max_lengths.z) do
				local len_z = get_len("z",k)
				pos1.z = pos_min.z + (k-1)*unej.max_lengths.z
				pos2.z = pos1.z + len_z
				local fname = i.."_"..j.."_"..k..".mts"
				local filename = path .. "/"..fname
				local schm = minetest.create_schematic(pos1, pos2, nil, filename)
				if schm == nil then
					minetest.chat_send_player(name, "UNEJ mod -!- Failed to create Minetest schematic", false)
					return
				end

				local pos1_save = {x=pos1.x + translation_X, y=pos1.y-pos_min.y, z=pos1.z + translation_Z}
				local pos2_save = {x=pos2.x + translation_X, y=pos2.y-pos_min.y, z=pos2.z + translation_Z}
				table.insert(data, {pos_min=pos1_save, pos_max=pos2_save, nom=fname})
			end
		end
	end
	if not unej.write_file(path .. "/" .. "data",data) then
		worldedit.player_notify(name, "The function occured an error, the data file is wrong")
		minetest.chat_send_player(name, unej.table_unpack(data))
	else
		worldedit.player_notify(name, "Saved Minetest schematic to " .. folder_name)
	end
end

unej.wmtsplace = function(name, param_y, folder_name, cmd, new)
	if new == nil then new = true end
  local path = minetest.get_worldpath() .. "/schems/" .. folder_name .."/"
  local geometry = unej.open_file( minetest.get_worldpath().."/geometry.dat")
	if not geometry then
		minetest.chat_send_player(name,"UNEJ mod -!- Erreur du chargement de worldpath/geometry.dat\nSi l'erreur persiste, vérifiez l'intégrité du fichier.")
		minetest.log("error", "UNEJ mod: Error when loading worldpath/geometry.dat.")
		return false
	end
  local data = unej.open_file( path.."data")
	if not data then
		minetest.chat_send_player(name,"UNEJ mod -!- Erreur du chargement de worldpath/schems/data\nSi l'erreur persiste, vérifiez l'intégrité du fichier.")
		minetest.log("error", "UNEJ mod: Error when loading worldpath/schems/data.")
		return false
	end
	if not data[2].pos_max then
		minetest.chat_send_player(name,"UNEJ mod -!- Erreur, le dossier spécifié a été créé depuis une version antérieure de ce mod et ne peut pas être utilisé.")
		return false
	end
	if not data[1].is_world then
		minetest.chat_send_player(name,"UNEJ mod -!- Erreur, le dossier spécifié n'est pas un fichier de monde.")
		return false
	end
	local saver = unej.open_file( minetest.get_worldpath().."/placement.dat")
	if not saver then
		minetest.chat_send_player(name,"UNEJ mod -!- Erreur du chargement de worldpath/placement.dat\nSi l'erreur persiste, vérifiez l'intégrité du fichier.")
		minetest.log("error", "UNEJ mod: Error when loading worldpath/placement.dat.")
		return false
	end
	local zone_name = folder_name.."_"..param_y

  local translation_X = (geometry["coordinatesCarto"][1][1] + geometry["coordinatesCarto"][3][1])/2
  local translation_Z = (geometry["coordinatesCarto"][1][2] +	geometry["coordinatesCarto"][3][2])/2

  minetest.chat_send_player(name,"UNEJ mod -!- Début de l'importation...")

  local pos_max = {x = data[1]["x"]-translation_X, y = param_y + data[1]["y"], z = data[1]["z"]-translation_Z}
	local history_path = minetest.get_worldpath() .. "/schems/history/"
  for i,v in ipairs(data) do
    if i ~= 1 then
      local pos = {x=v.pos_min.x-translation_X, y = v.pos_min.y + param_y, z=v.pos_min.z-translation_Z}
      if i == 2 and new then
        pos_min = pos
				unej.history_switch(cmd)
				os.remove(history_path..unej.history[1].folder_name)
				minetest.mkdir(history_path..unej.history[1].folder_name)
      end
			local pos2 = {x=v.pos_max.x-translation_X, y = v.pos_max.y + param_y, z=v.pos_max.z-translation_Z}
			local schm_hist = minetest.create_schematic(pos, pos2, nil, history_path..unej.history[1].folder_name.."/".. #unej.history[1].pos + 1 ..".mts")
			table.insert(unej.history[1].pos, pos)
			if not schm_hist then
				minetest.chat_send_player(name,"UNEJ mod -!- failed to save history schematic, please undo if a part of the world has been placed")
				return false
			end
			local schm = minetest.place_schematic(pos, path..v.nom)
      if schm == nil then
        minetest.chat_send_player(name,"UNEJ mod -!- failed to place Minetest schematic")
        return false
			end
			unej.write_file(minetest.get_worldpath() .. "/schems/history/history.dat",unej.history)
    end
  end
	saver[zone_name] = {folder_name=folder_name, creation_time = os.date("%Y/%m/%d %H:%M:%S"), translation_X = translation_X, translation_Z = translation_Z, angleDegres = geometry["angleDegres"], pos_min = pos_min, pos_max = pos_max, protected = false}
  minetest.chat_send_player(name,"UNEJ mod -!- placed Minetest schematic " .. folder_name)
  return pos_min, pos_max, saver
end


minetest.register_chatcommand("/worldcreate", {
	params = "<folder_name> [min_y] [max_y]",
	description = "Save the current world using the Minetest "..
		"Schematic format to \"(world folder)/schems/<folder>/*_*_*.mts\"",
	privs = {unej_admin=true},
	func = function(name, param)
	local params = {}
	for p in string.gmatch(param, "[^%s]+") do
		table.insert(params, p)
	end
	if #params == 0 then
		minetest.chat_send_player(name, "IRI -!- Error with parameters")
		return false
	end
	unej.wmtscreate(name,params)
	end,
})


minetest.register_chatcommand("/worldplace", {
	params = "<y> <file>",
	description = "Load nodes from \"(world folder)/schems/<file>/*.mts\"",
	privs = {unej_admin=true},
	func = function(name, param)
		local cmd = "//worldplace " .. param
    local found, _, param_y, folder_name = param:find("([+-]?%d+)%s+(%S+)$")
    if found == nil then
      minetest.chat_send_player(name,"UNEJ mod -!- Erreur dans les paramètres")
      return
    end
	  pos_min, pos_max, saver = unej.wmtsplace(name, param_y, folder_name,cmd)
		if not saver then
			return false
		end
		if not unej.write_file(minetest.get_worldpath().."/placement.dat", saver) then
			minetest.chat_send_player(name,"UNEJ mod -!- Erreur lors de la sauvegarde automatique.")
			minetest.chat_send_player(name,"UNEJ mod -!- Voici les infos reçues :")
			minetest.chat_send_player(name,unej.table_unpack(saver))
		end
	end,
})

minetest.register_chatcommand("/worldpap", {
	params = "<y> <folder> <area_name> [gap_y] [folder2] [area_name2] ...",
	description = "Load nodes from \"(world folder)/schems/<folder>/*.mts\", make an invisible barrier all around and protect all by naming the area <area_name>",
	privs = {unej_admin=true},
	func = function(name, param)
		local cmd = "//worldpap ".. param
		local params = {}
		for p in string.gmatch(param, "[^%s]+") do
			table.insert(params, p)
		end
		local param_y = tonumber(params[1])
		local folders_names = {params[2]}
		local areas_names = {params[3]}
		local fill_in = true
		if params[4]=="false" then
			fill_in = false
			table.remove(params,4)
		end
		local gap_y = 0
		if params[4] then
			if not tonumber(params[4]) then
				table.insert(params, 4, 512)
			end
			gap_y = tonumber(params[4])
			local len = #params
			for i = 3, math.floor(len/2) do
				if params[i*2-1] and params[i*2] then
					table.insert(folders_names, params[i*2-1])
					table.insert(areas_names, params[i*2])
				end
			end
		end
    if #params < 3 then
			minetest.chat_send_player(name,"IRI -!- Error in parameters, try again")
		end
		for i,folder_name in ipairs(folders_names) do
			y_base = (i-1)*gap_y + param_y
			area_name = areas_names[i]
	    -- Placement de la plaque
			if i == 1 then
		  	pos_min, pos_max, saver = unej.wmtsplace(name, y_base, folder_name,cmd)
			else
				pos_min, pos_max, saver = unej.wmtsplace(name, y_base, folder_name,cmd,false)
			end
	    if not pos_min then
	      return
	    end

			-- Protection de la Zone
			id = unej.protect(pos_min,pos_max,name,area_name,fill_in)
			saver[folder_name.."_"..param_y]["protected"] = true
			saver[folder_name.."_"..param_y]["area_owner"] = name
			saver[folder_name.."_"..param_y]["area_name"] = area_name
			saver[folder_name.."_"..param_y]["area_id"] = id

			if not unej.write_file(minetest.get_worldpath().."/placement.dat", saver) then
				minetest.chat_send_player(name,"UNEJ mod -!- Erreur lors de la sauvegarde automatique.")
				minetest.chat_send_player(name,"UNEJ mod -!- Voici les infos reçues :")
				minetest.chat_send_player(name,unej.table_unpack(saver))
			else
				minetest.chat_send_player(name,"L'importation de "..folder_name.." à l'altitude "..y_base.." est terminée !")
			end
		end
		if gap_y then
			minetest.chat_send_player(name,"Toutes les importations se sont terminées avec succès")
		end
	end,
})
