local conf_path = minetest.get_worldpath() .. "/unej/manage_privs.conf"
local registered_priv_groups = {}

-- Syntax of priv_groups in conf file is list-like for user-friendliness: ["foo", "bar"] -> { 1: "foo", 2: "bar" }
-- This converts it to table-like for convenience : { "foo": true, "bar": true }
-- It also performs validity checks and registers a specific privilege for each priv_group
local conf_priv_groups = (unej.open_file(conf_path, false) or { priv_groups= {} })["priv_groups"]
if type(conf_priv_groups) == "table" then
	for priv_group_name, priv_group_list in pairs(conf_priv_groups) do
		if type(priv_group_list) == "table" then
			if not registered_priv_groups[priv_group_name] then
				registered_priv_groups[priv_group_name] = {}
				for index, priv_name in pairs(priv_group_list) do
					if type(priv_name) == "string" then
						registered_priv_groups[priv_group_name][priv_name] = true
					else
						registered_priv_groups[priv_group_name] = nil
						minetest.log("warning","UNEJ mod -!- invalid privilege: "
						.. priv_group_name .. " [" .. tostring(index) .. "]. Priv_group dismissed")
						break
					end
				end
				if registered_priv_groups[priv_group_name] then
					minetest.register_privilege(
						"u_grant_" .. priv_group_name,
						{ description = "may grant" .. priv_group_name,
						give_to_singleplayer = false })
				end
			else
				minetest.log("warning", "UNEJ mod -!- priv_group already registered: ".. priv_group_name .. ".")
			end
		else
			minetest.log("warning","UNEJ mod -!- invalid priv_group: " .. priv_group_name .. ".")
		end
	end
else
	minetest.log("warning","UNEJ mod -!- could not load registered priv_groups from " .. conf_path .. ".")
end

local function authorized_priv_group_names(name)
	local priv_group_names = {}
	for priv_group_name in pairs(registered_priv_groups) do
		if minetest.check_player_privs(name, "u_grant")
			or minetest.check_player_privs(name, "u_grant_" .. priv_group_name) then
			priv_group_names[priv_group_name] = true
		end
	end
	return priv_group_names
end

local function authorize_priv(name, priv_name)
	if minetest.registered_privileges[priv_name] then
		if minetest.check_player_privs(name, "u_grant")
			or minetest.check_player_privs(name, "u_grant_" .. priv_name) then
			return true
		end
		for authorized_priv_group in pairs(authorized_priv_group_names(name)) do
			for priv in pairs(registered_priv_groups[authorized_priv_group]) do
				if priv == priv_name then return true end
			end
		end
	end
	return false
end

local function authorize_priv_group(name, priv_group_name)
	return (minetest.check_player_privs(name, "u_grant")
		or minetest.check_player_privs(name, "u_grant_" .. priv_group_name))
		and registered_priv_groups[priv_group_name]
end

local function provision_privs(name, query)
	if authorize_priv(name, query) then return { [query] = true } end
	if authorize_priv_group(name, query) then return registered_priv_groups[query] end
	return nil, "Privilege or priv_group " .. query .. " not found or not allowed"
end

local function toggle_privs(player_names, toggled_privs, new_value)
	local names_by_outcome = {}
	if not next(toggled_privs) then return false, "No privilege to grant." end
	if not next(player_names) then return false, "No player to affect." end
	for player_name in pairs(player_names) do
		local player_privs, some_success, some_failure = minetest.get_player_privs(player_name), false, false
		for priv_name in pairs(toggled_privs) do
			if player_privs[priv_name] == new_value then
				some_failure = true
			else
				player_privs[priv_name] = new_value
				some_success = true
			end
		end
		minetest.set_player_privs(player_name, player_privs)
		unej.register_outcome(names_by_outcome, player_name,
			(some_success and (some_failure and "Some" or "All") or "None of the")
			.. " privilege(s) " .. (new_value and "granted" or "revoked"))
	end
	return true, unej.summarize_iteration(names_by_outcome)
end

local u_grant_description="Extends the built-in /grant command. Recipients can be a player or a faction.\
One can grant a single privilege or a group of privileges specified in <world directory>/unej/manage_privs.conf\
example syntax for conf file: { \"priv_groups\": { \"basic\": { \"fly\": \"true\", \"interact\": \"true\" } } }"

minetest.register_chatcommand("u_grant", {
	params = "<player_name|faction [and_me]> <privilege|privs_group>",
	description = u_grant_description,
	func = function(name, params)
		local player_names, query = unej.process_names(name, params, "u_grant")
		if not player_names then return false, query end
		local privs, reason = provision_privs(name, query)
		if not privs then return false, reason end
		return toggle_privs(player_names, privs, true)
	end,
})

minetest.register_chatcommand("u_grantme", {
	params = "<privilege|privs_group>",
	description = "Same as u_grant, but targets you",
	func = function(name, query)
		local privs, reason = provision_privs(name, query)
		if not privs then return false, reason end
		return toggle_privs({ [name] = true }, privs, true)
	end,
})

minetest.register_chatcommand("u_revoke", {
	params = "<player_name|factions [and_me]> <privs|privs_group>",
	description = "Revoke a given privilege or a group of privileges from a player or a faction",
	func = function(name, params)
		local player_names, query = unej.process_names(name, params, "u_grant")
		if not player_names then return false, query end
		local privs, reason = provision_privs(name, query)
		if not privs then return false, reason end
		return toggle_privs(player_names, privs, nil)
	end,
})

minetest.register_chatcommand("u_priv_groups", {
	params = "[<priv_group>]",
	description = "List available priv_groups (if no argument is given) or the contents of designated priv_group",
	func = function(name, params)
		local priv_group_name = string.match(params, "([%S]+)")
		local summary
		if priv_group_name then -- list priv_group contents
			if authorize_priv_group(name, priv_group_name) then
				summary = "Priv_group " .. priv_group_name .. " consists of:"
				for priv in pairs(registered_priv_groups[priv_group_name]) do
					summary = summary .. " " .. priv
				end
			else
				return false, "Priv_group " .. priv_group_name .. " is not available"
			end
		else --list packages
			summary = "Available priv_groups:"
			for authorized_priv_group in pairs(authorized_priv_group_names(name)) do
				summary = summary .. " " .. authorized_priv_group
			end
		end
		return true, summary
	end
})
