local conf_path = minetest.get_worldpath() .. "/unej/manage_give.conf"
local registered_packages, conf_packages = {}, {}

local function filter_registered_stacks(stack_list)
	local copy, index, item_stack = {}, next(stack_list)
	while index do
		if minetest.registered_items[item_stack:get_name()] then table.insert(copy, item_stack) end
		index, item_stack = next(stack_list, index)
	end
	return copy
end

setmetatable(registered_packages,{
	__index = function(parent_table, package_name)
		if conf_packages[package_name] then
			local registered_stacks = filter_registered_stacks(conf_packages[package_name])
			parent_table[package_name] = registered_stacks
			return registered_stacks
		else
			return nil
		end
	end
})

local function insert_split_stack(stack_list, item_stack)
	local count = item_stack:get_count()
	local max = item_stack:get_stack_max()
	while count > 0 do
		local copy = ItemStack(item_stack)
		if count > max then
			copy:set_count(max)
			count = count - max
		else
			copy:set_count(count)
			count = 0
		end
		table.insert(stack_list, copy)
	end
end

local function authorized_package_names(name)
	local package_names = {}
	if minetest.check_player_privs(name, "u_give") then
		for package_name, _ in pairs(conf_packages) do
			package_names[package_name] = true
		end
	else
		for priv in pairs(minetest.get_player_privs(name)) do
			local package_name = string.match(priv, "u_give_" .. "(%S+)")
			if package_name and registered_packages[package_name] then
				package_names[package_name] = true
			end
		end
	end
	return package_names
end

local function authorize_item(name, item_name)
	if minetest.registered_items[item_name] then
		if minetest.check_player_privs(name, "u_give") then return true end
		for authorized_package in pairs(authorized_package_names(name)) do
			for _, stack in ipairs(registered_packages[authorized_package]) do
				if stack:get_name() == item_name then return true end
			end
		end
	end
	return false
end

local function authorize_package(name, package_name)
	return (minetest.check_player_privs(name, "u_give")
		or minetest.check_player_privs(name, "u_give_" .. package_name))
		and registered_packages[package_name]
end

local function provision_stacks(name, query)
	local item_stack, stack_list = ItemStack(query), {}
	if authorize_item(name, item_stack:get_name()) then
		insert_split_stack(stack_list, item_stack)
		return stack_list
	end
	if authorize_package(name, query) then return registered_packages[query] end
	return nil, "Item or package " .. query .. " not found or not allowed."
end

local function iterate_inventory_action(player_names, action)
	local names_by_outcome, player, inventory, outcome = {}
	if not next(player_names) then return "No player was affected." end
	for player_name in pairs(player_names) do
		player = minetest.get_player_by_name(player_name)
		if player then
			inventory = player:get_inventory()
			outcome = inventory and action(inventory) or "inventory error"
		else
			outcome = "disconnected"
		end
		unej.register_outcome(names_by_outcome, player_name, outcome)
	end
	return unej.summarize_iteration(names_by_outcome)
end

local function gen_give_stack_list(stack_list)
	return function(inventory)
		if not next(stack_list) then return "Nothing to recieve" end
		local some_success, some_failure = false, false
		for _, item_stack in ipairs(stack_list) do
			local leftover = inventory:add_item("main", item_stack)
			if leftover:is_empty() then
				some_success = true
			elseif leftover:get_count() == item_stack:get_count() then
				some_failure = true
			else
				some_success, some_failure = true, true
			end
		end
		return (some_success and (some_failure and "Some" or "All") or "None of the") .. " items recieved"
	end
end

local function clear_inventory(inventory)
	local was_empty = true
	for list_name, _ in pairs(inventory:get_lists()) do
		if not inventory:is_empty(list_name) then was_empty = false end
		inventory:set_list(list_name, {})
	end
	return "Inventory " .. (was_empty and "already empty" or "emptied")
end

local function gen_equip(stack_list)
	return function(inventory)
		return clear_inventory(inventory) .. ", " .. gen_give_stack_list(stack_list)(inventory)
	end
end

-- registered_packages initialization
-- the following converts the json data read from configuration file into a properly formatted table 'conf_packages'
-- it also registers a specific privilege for each package
-- since item registration cannot be checked at mod init time, it sets a matatable
-- to fetch and check the contents of each package from 'conf_packages' once only, at runtime.

local conf_data = (unej.open_file(conf_path, false) or { packages = {} })["packages"]
if type(conf_data) == "table" then
	for package_name, package in pairs(conf_data) do
		if type(package) == "table" then
			if not conf_packages[package_name] then
				conf_packages[package_name] = {}
				for index, itemstring in ipairs(package) do
					if type(itemstring) == "string" then
						insert_split_stack(conf_packages[package_name], ItemStack(itemstring))
					else
						conf_packages[package_name] = nil
						minetest.log("warning","UNEJ mod -!- invalid item: "
						.. package_name .. " [" .. tostring(index) .. "]. Package dismissed")
						break
					end
				end
				if conf_packages[package_name] then
						minetest.register_privilege(
							"u_give_" .. package_name,
							{description = "Holder has access to contents of " .. package_name,
							give_to_singleplayer = false})
						minetest.register_privilege(
							"u_grant_u_give_" .. package_name,
							{description = "Holder may grant access to contents of " .. package_name,
							give_to_singleplayer = false})
				end
			else
				minetest.log("warning","UNEJ mod -!- package already registered: " .. package_name .. ".")
			end
		else
			minetest.log("warning","UNEJ mod -!- invalid package: " .. package_name .. ".")
		end
	end
else
	minetest.log("warning","UNEJ mod -!- could not load registered packages from " .. conf_path .. ".")
end


local u_give_description = "This is an extension of the basic /give command.\
<amount>: 1-65535, defaults to 1 is optional, only affects stackable nodes, has no effect otherwise (e.g. tools)\
<wear>: 0-65535, defaults to 0 (intact), requires amount to be specified, only affects tools.\
Configuration file (<world directory>" .. conf_path .. ") example: \
{ \"packages\": {\"my_package\": [ \"default:brick 100\", \"default:pick_steel 1 30000\" ] } }"

minetest.register_chatcommand("u_give", {
	params = "<player|faction [and_me]> <identifier [<amount>[ <wear>]]|package>",
	description = u_give_description,
	func = function(name, params)
		local player_names, extra = unej.process_names(name, params, "u_give")
		if not player_names then return false, extra end
		local stack_list, summary = provision_stacks(name, extra)
		if not stack_list then return false, summary end
		local give_stack_list = gen_give_stack_list(stack_list)
		return true, iterate_inventory_action(player_names, give_stack_list)
	end
})

minetest.register_chatcommand("u_giveme", {
	params = "<identifier [<amount>[ <wear>]]|package>",
	description = "This is an extension of the basic /give command. See /help u_give for contents syntax",
	func = function(name, params)
		local stack_list, summary = provision_stacks(name, params)
		if not stack_list then return false, summary end
		local give_stack_list = gen_give_stack_list(stack_list)
		return true, iterate_inventory_action({[name] = true}, give_stack_list)
	end
})

minetest.register_chatcommand("u_empty_inventory", {
	params = "<player|faction [and_me]>",
	description = "Empty inventory of designated player(s)",
	func = function(name, params)
		local player_names, extra = unej.process_names(name, params, "u_empty_inventory")
		if not player_names then return false, extra end
		return true, iterate_inventory_action(player_names, clear_inventory)
	end
})

minetest.register_chatcommand("u_equip", {
	params = "<player|faction [and_me]> <identifier [<amount>[ <wear>]]|package>",
	description = "same as u_give, but replaces player inventory instead of adding to it (see /help u_give)",
	func = function(name, params)
		local player_names, extra = unej.process_names(name, params, "u_empty_inventory")
		if not player_names then return false, extra end
		local stack_list, summary = provision_stacks(name, extra)
		if not stack_list then return false, summary end
		local equip = gen_equip(stack_list)
		return true, iterate_inventory_action(player_names, equip)
	end
})

minetest.register_chatcommand("u_packages", {
	params = "[<package>]",
	description = "List available packages (if no argument is given) or the contents of designated package",
	func = function(name, param)
		local package_name = string.match(param, "(%S+)")
		local summary
		if package_name then
			if authorize_package(name, package_name) then -- list package contents
				summary = "Package " .. package_name .. " contains:"
				for _, item_stack in ipairs(registered_packages[package_name]) do
					summary = summary .. " [" .. item_stack:get_name() .. " " .. item_stack:get_count() .. "]"
				end
			else
				return false, "Package " .. package_name .. " is not available."
			end
		else --list packages
			summary = "Available packages:"
			for authorized_package_name in pairs(authorized_package_names(name)) do
				summary = summary .. " " .. authorized_package_name
			end
		end
		return true, summary
	end
})
